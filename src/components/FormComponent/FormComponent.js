import React, {Fragment} from 'react';

const Form = (props) => {
    return (
        <Fragment>
            <input className="formitem" onChange={props.changeName} type="text"  placeholder='Item name'/>
            <input className="formitem" onChange={props.changeCost} type="text" placeholder='Cost'/>
            <button className="formbtn" onClick={props.addTask}>Add</button>
        </Fragment>
    )
};

export default Form;