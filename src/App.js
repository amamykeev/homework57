import React, { Component } from 'react';
import './App.css';
import Form from './components/FormComponent/FormComponent';
import Item from './components/ItemsComponent/ItemComponent';

class App extends Component {

  state = {
    tasks: [
      {name: 'Bottle of water', cost: 25},
      {name: 'Milk', cost: 50},
    ],
    cost: 0,
    itemName: ''
  };

  changeCost = (event) => {
    this.setState({
      cost: parseInt(event.target.value)
    })
  };

  changeItemName = (event) => {
    this.setState({
      itemName: event.target.value
    })
  }

  addTask = () => {
    const tasks = [...this.state.tasks]
    const newTask = {
      name: this.state.itemName,
      cost: this.state.cost
    }
    tasks.push(newTask);
    this.setState({
      tasks: tasks,
      itemName: '',
      cost: 0
    })
  };

  // removeItem = () => {
  //   this.setState({
  //     tasks: this.state.tasks.filter((delete) => tasks.key ! == key)
  //   });
  // }

  render() {
    return (
      <div className="App">
        <header>
          <h1>Personal data accounting!</h1>
        </header>
        <div className="Form">
        <Form
            addTask={() => this.addTask()}
            changeCost={(event) => this.changeCost(event)}
            changeName={(event) => this.changeItemName(event)}
        />
        </div>
        <Item lolkek={this.state.tasks}/>
      </div>
    );
  }
}

export default App;
